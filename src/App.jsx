import React from "react";
import NavBar from "./component/NavBar"
import TileTech from "./component/TileTech";
import WelcomeText from "./component/WelcomeText";
import mobile from "./assets/img/mobile.png"
import locker from "./assets/img/locker.png"
import ProjectContainer from "./component/ProjectContainer";

export default function App (){
  return(
    <div>
      <NavBar />
      <div className="welcome">
        <WelcomeText />
        <div className="welcome-tile">
          <TileTech title="developpeur mobile" img={mobile} color="#64a6f8"/>
          <TileTech title="expert en cryptographie" img={locker} color="#c7d1d9"/>
        </div>
      </div>
      <span className="separate"></span>
      <ProjectContainer />
    </div>
  )
}