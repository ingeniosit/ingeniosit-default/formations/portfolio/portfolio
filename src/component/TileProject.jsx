import react from "react";
import "../style/TileProject.scss"

export default function TileProject ({title,description,href}) {
    return(
        <a href={href} className="tile-project">
            <h4>{title}</h4>
            <p>{description}</p>
        </a>
    )
}