import react from "react";
import TileProject from "./TileProject";
import "../style/ProjectContainer.scss"

export default function ProjectContainer (){
    return(
        <div className="project-container">
            <TileProject title="projet1" description="Donec tortor urna, tristique sit amet blandit vitae, consequat at tortor. Donec vitae eros eget augue sagittis ultrices. Maecenas dignissim mauris ante, at ullamcorper mauris fringilla non. Vestibulum sit amet risus neque. Pellentesque ligula erat, efficitur a quam a, viverra tempor urna. Fusce eu est vel turpis sollicitudin mattis. Nullam bibendum mattis dictum. In hac habitasse platea dictumst. " href="https://google.com" />
            <TileProject title="projet2" description="Donec tortor urna, tristique sit amet blandit vitae, consequat at tortor. Donec vitae eros eget augue sagittis ultrices. Maecenas dignissim mauris ante, at ullamcorper mauris fringilla." href="https://google.com" />
            <TileProject title="projet3" description="Donec tortor urna, tristique sit amet blandit vitae, consequat at tortor. Donec vitae eros eget augue sagittis ultrices. Maecenas dignissim mauris ante, at ullamcorper mauris fringilla." href="https://google.com" />
            <TileProject title="projet4" description="Donec tortor urna, tristique sit amet blandit vitae, consequat at tortor. Donec vitae eros eget augue sagittis ultrices. Maecenas dignissim mauris ante, at ullamcorper mauris fringilla. Mauris eu cursus ex. Mauris imperdiet turpis nulla, id viverra orci pharetra at. Nunc nisl odio, consequat non venenatis volutpat, luctus ut sem. Nullam aliquam nisi mauris, in interdum diam commodo nec. Suspendisse nec urna metus. Quisque rutrum leo libero, vitae porttitor risus lacinia sit amet. Donec luctus dui scelerisque nibh elementum aliquet ut ut est. Nunc malesuada consequat massa quis efficitur. Nullam ut libero posuere, pretium eros in, iaculis nibh. " href="https://google.com" />
        </div>
    )
}