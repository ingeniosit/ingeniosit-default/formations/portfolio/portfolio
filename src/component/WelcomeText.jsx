import react from "react";
import humaans_img from "../assets/img/humaans.png"
import "../style/WelcomeText.scss"

export default function WelcomeText (){
    return(
        <div className="welcome-text">
            <div className="welcome-text-left">
                <h4>Bonjour ! <br/> je m'appelle lucas</h4>
                <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. In pulvinar sapien at massa fringilla, ac interdum orci pellentesque. Nullam purus augue, auctor a lacinia a, vulputate in magna. Nulla facilisi. Praesent id ante non ligula faucibus mattis. </p>
                <button>contactez moi</button>
            </div>
            <img src={humaans_img} alt="belle illutsration" />
        </div>
    )
}