import react from "react";
import "../style/TileTech.scss"

export default function TileTech ({title,img,color}){
    return(
        <div className="TileTech" style={{"backgroundColor" : color}}>
            <h4>{title}</h4>
            <img src={img} alt="jolie illustration" />
        </div>
    )
}